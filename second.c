#include <stdio.h>
#include "func.h"
#include <time.h>
#include <conio.h>

int n=5;
int m=6;
int B[5][6];

void in()
{
    int i, j;
    printf("\n\nOriginal matrix B:\n\n");
    for (i=0;i<n;i++)
        for (j=0;j<m;j++)
            {B[i][j]=rand()%100-50;
            printf("B[%d][%d]=%d\n", i, j, B[i][j]);}
}

void out()
{
    int i, j;
    printf("\n\nChanged matrix B:\n\n");
    for (i=0;i<n;i++)
        for (j=0;j<m;j++)
        {
            if (B[i][j]%2==0)
                B[i][j]=B[i][j]*B[i][j];
            printf("B[%d][%d]=%d\n", i, j, B[i][j]);
        }
}
