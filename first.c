#include <stdio.h>
#include "func.h"
#include <time.h>
#include <conio.h>

int n=30;
int A[30];

void in()
{
    printf("\n\nOriginal array A:\n\n");
    int i;
    for (i=0;i<n;i++)
        {A[i]=rand()%100-50;
        printf("A[%d]=%d\n", i, A[i]);}
}

void out()
{
    int i;
    printf("\n\nChanged array A:\n\n");
    for (i=0;i<n;i++)
    {
        if (A[i]%2==0)
            A[i]=A[i]*A[i];
        printf("A[%d]=%d\n", i, A[i]);
    }
}
