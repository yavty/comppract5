#ifndef WIN32
		#include <dlfcn.h>
#else
		#include <windows.h>
#endif

#include "load.h"
#include "func.h"
#include <stdio.h>

void LoadRun(const char * const s) {
    void * lib;
    void (*func)(void);
    lib = LoadLibrary(s);
    if (!lib)
    {
        printf("cannot open library '%s'\n", s);
        return;
    }
    func = (void (*)(void))GetProcAddress((HINSTANCE)lib, "in");
    if (func == NULL)
    {
         printf("cannot load function in\n");
    }
    else {func();}
    func = (void (*)(void))GetProcAddress((HINSTANCE)lib, "out");
    if (func == NULL)
    {
        printf("cannot load function out\n");
    }
    else {func();}
       FreeLibrary((HINSTANCE)lib);
    }
